from pytest import raises

from respice.cachetools import LRUCache


def test_basic():
    cache = LRUCache(2)
    assert len(cache) == 0

    cache[0] = 'a'
    assert len(cache) == 1

    cache[1] = 'b'
    assert len(cache) == 2

    cache[2] = 'c'
    assert len(cache) == 2

    assert 0 not in cache
    assert 1 in cache
    assert 2 in cache


def test_stealth_methods():
    cache = LRUCache(4)

    cache[0] = 'a'
    assert len(cache) == 1
    assert 0 in cache

    cache.set_stealth(1, 'b')
    assert len(cache) == 2
    assert 0 in cache
    assert 1 in cache

    cache[2] = 'c'
    assert len(cache) == 3
    assert 0 in cache
    assert 1 in cache
    assert 2 in cache

    cache.set_stealth(3, 'd')
    assert len(cache) == 4
    assert 0 in cache
    assert 1 in cache
    assert 2 in cache
    assert 3 in cache

    cache.get_stealth(3)

    cache[4] = 'e'
    assert len(cache) == 4
    assert 0 in cache
    assert 1 in cache
    assert 2 in cache
    assert 3 not in cache
    assert 4 in cache

    cache.set_stealth(5, 'f')
    assert len(cache) == 4
    assert 0 in cache
    assert 1 in cache
    assert 2 in cache
    assert 3 not in cache
    assert 4 in cache
    assert 5 not in cache


def test_iterators():
    cache = LRUCache(4)

    cache[0] = 'a'
    cache[1] = 'b'
    cache[2] = 'c'

    # Consecutive equations should not change order, cache LRU values are not affected by iteration.

    assert list(cache.items()) == [(2, 'c'), (1, 'b'), (0, 'a')]
    assert list(cache.items()) == [(2, 'c'), (1, 'b'), (0, 'a')]

    assert list(cache.keys()) == [2, 1, 0]
    assert list(cache.keys()) == [2, 1, 0]
    assert list(cache.keys()) == list(cache)

    assert list(cache.values()) == ['c', 'b', 'a']
    assert list(cache.values()) == ['c', 'b', 'a']


def test_last():
    cache = LRUCache(4)

    with raises(KeyError):
        cache.last()

    cache.set_stealth(0, 'a')
    assert cache.last() == (0, 'a')

    cache[1] = 'b'
    assert cache.last() == (1, 'b')

    cache.set_stealth(2, 'c')
    assert cache.last() == (1, 'b')


def test_deletion():
    cache = LRUCache(2)

    cache[0] = 7
    assert 0 in cache
    del cache[0]
    assert 0 not in cache

    with raises(KeyError):
        del cache[0]


def test_clear():
    cache = LRUCache(4)

    cache[0] = 'a'
    cache[1] = 'b'
    cache[2] = 'c'

    assert len(cache) == 3

    cache.clear()

    assert len(cache) == 0


def test_full():
    cache = LRUCache(2)
    assert not cache.full

    cache[0] = 'a'
    assert not cache.full

    cache[1] = 'b'
    assert cache.full

    cache[2] = 'c'
    assert cache.full
