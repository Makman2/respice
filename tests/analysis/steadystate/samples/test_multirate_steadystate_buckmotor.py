from math import sin, pi

import numpy as np
import pytest
from pytest import approx

from respice.analysis import Circuit
from respice.components import VoltageSourceRectangular, L, C
from respice.components.Motor import Motor


@pytest.mark.slow
def test_multirate_steadystate_buckmotor(tstdata):
    E = 100
    f_converter = 100e3
    f_torque = 10

    src = VoltageSourceRectangular(amplitude=E / 2, frequency=f_converter, offset=E / 2, duty=0.3)
    l1 = L(420e-6)
    c1 = C(38e-6)
    motor = Motor(
        R=0.425,
        L=0.00378,
        KT=0.466,
        KE=0.466,
        J=0.02255,
        D=0.001,
        TL=lambda t: 5 + sin(2 * pi * f_torque * t),
    )

    circuit = Circuit()
    circuit.add(src, 0, 1)
    circuit.add(motor, 0, 'out')
    circuit.add(c1, 0, 'out')
    circuit.add(l1, 'out', 1)

    T_converter = 1 / f_converter
    T_torque = 1 / f_torque

    simulation = circuit.multirate_steadystate(0.1 * T_converter, T_converter, T_torque)
    simulation.wait()

    testdata = tstdata.load('motor.csv')

    assert simulation.t() == approx(testdata['t'])
    assert simulation.v(src) == approx(testdata['v(E)'])
    assert simulation.i(src) == approx(testdata['i(E)'])
    assert simulation.v(motor) == approx(testdata['v(motor)'])
    assert simulation.i(motor) == approx(testdata['i(motor)'])

    motorstates = np.transpose(simulation.s(motor))
    assert motorstates[-1] == approx(testdata['state(motor)[3]'])
