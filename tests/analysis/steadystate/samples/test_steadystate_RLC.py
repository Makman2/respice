import pytest
from pytest import approx

from respice.analysis import Circuit
from respice.components import VoltageSourceAC, R, L, C


def test_rlc_filter(subtests, tstdata):
    f = 1000
    T = 1 / f
    steps = [10, 20]

    for s in steps:
        with subtests.test(msg='vary steps s per period T', s=s):
            e1 = VoltageSourceAC(amplitude=10.0, frequency=f)
            r1 = R(50)
            l1 = L(1e-2, 2)
            c1 = C(1e-5)
            r_load = R(1000)

            circuit = Circuit()
            circuit.add(e1, 0, 1)
            circuit.add(l1, 1, 2)
            circuit.add(r1, 2, 0)
            circuit.add(c1, 2, 0)
            circuit.add(r_load, 2, 0)

            simulation = circuit.steadystate(T, s)
            simulation.wait()

            testdata = tstdata.load(f'filter_steadystate_s{s}.csv')

            assert simulation.v(e1) == approx(testdata['v(E1)'], 1e-03)
            assert simulation.i(e1) == approx(testdata['i(E1)'], 1e-03)
            assert simulation.v(r1) == approx(testdata['v(R1)'], 1e-03)
            assert simulation.i(r1) == approx(testdata['i(R1)'], 1e-03)
            assert simulation.v(l1) == approx(testdata['v(L1)'], 1e-03)
            assert simulation.i(l1) == approx(testdata['i(L1)'], 1e-03)
            assert simulation.v(c1) == approx(testdata['v(C1)'], 1e-03)
            assert simulation.i(c1) == approx(testdata['i(C1)'], 1e-03)
            assert simulation.v(r_load) == approx(testdata['v(R_load)'], 1e-03)
            assert simulation.i(r_load) == approx(testdata['i(R_load)'], 1e-03)

            # Resimulate another period. Steady-state solution must stay the same for consequent periods.
            simulation2 = circuit.simulate(T, 2*T, s)
            simulation2.wait()

            assert simulation2.v(e1) == approx(testdata['v(E1)'], 1e-03)
            assert simulation2.i(e1) == approx(testdata['i(E1)'], 1e-03)
            assert simulation2.v(r1) == approx(testdata['v(R1)'], 1e-03)
            assert simulation2.i(r1) == approx(testdata['i(R1)'], 1e-03)
            assert simulation2.v(l1) == approx(testdata['v(L1)'], 1e-03)
            assert simulation2.i(l1) == approx(testdata['i(L1)'], 1e-03)
            assert simulation2.v(c1) == approx(testdata['v(C1)'], 1e-03)
            assert simulation2.i(c1) == approx(testdata['i(C1)'], 1e-03)
            assert simulation2.v(r_load) == approx(testdata['v(R_load)'], 1e-03)
            assert simulation2.i(r_load) == approx(testdata['i(R_load)'], 1e-03)


@pytest.mark.slow
def test_rlc_filter_multirate(subtests, tstdata):
    f = 1000
    T = 1 / f
    steps = [10, 20, 50]

    for s in steps:
        with subtests.test(msg='vary steps s per period T', s=s):
            e1 = VoltageSourceAC(amplitude=10.0, frequency=f)
            e2 = VoltageSourceAC(amplitude=10.0, frequency=3 * f)
            r1 = R(50)
            l1 = L(1e-2, 2)
            c1 = C(1e-5)
            r_load = R(1000)

            circuit = Circuit()
            circuit.add(e1, 0, 1)
            circuit.add(e2, 1, 2)
            circuit.add(l1, 2, 3)
            circuit.add(r1, 3, 0)
            circuit.add(c1, 3, 0)
            circuit.add(r_load, 3, 0)

            simulation = circuit.steadystate(T, s)
            simulation.wait()

            testdata = tstdata.load(f'filter_multirate_steadystate_s{s}.csv')

            assert simulation.v(e1) == approx(testdata['v(E1)'], 1e-03)
            assert simulation.i(e1) == approx(testdata['i(E1)'], 1e-03)
            assert simulation.v(e2) == approx(testdata['v(E2)'], 1e-03)
            assert simulation.i(e2) == approx(testdata['i(E2)'], 1e-03)
            assert simulation.v(r1) == approx(testdata['v(R1)'], 1e-03)
            assert simulation.i(r1) == approx(testdata['i(R1)'], 1e-03)
            assert simulation.v(l1) == approx(testdata['v(L1)'], 1e-03)
            assert simulation.i(l1) == approx(testdata['i(L1)'], 1e-03)
            assert simulation.v(c1) == approx(testdata['v(C1)'], 1e-03)
            assert simulation.i(c1) == approx(testdata['i(C1)'], 1e-03)
            assert simulation.v(r_load) == approx(testdata['v(R_load)'], 1e-03)
            assert simulation.i(r_load) == approx(testdata['i(R_load)'], 1e-03)

            # Resimulate another period. Steady-state solution must stay the same for consequent periods.
            simulation2 = circuit.simulate(T, 2 * T, s)
            simulation2.wait()

            assert simulation2.v(e1) == approx(testdata['v(E1)'], 1e-03)
            assert simulation2.i(e1) == approx(testdata['i(E1)'], 1e-03)
            assert simulation2.v(e2) == approx(testdata['v(E2)'], 1e-03)
            assert simulation2.i(e2) == approx(testdata['i(E2)'], 1e-03)
            assert simulation2.v(r1) == approx(testdata['v(R1)'], 1e-03)
            assert simulation2.i(r1) == approx(testdata['i(R1)'], 1e-03)
            assert simulation2.v(l1) == approx(testdata['v(L1)'], 1e-03)
            assert simulation2.i(l1) == approx(testdata['i(L1)'], 1e-03)
            assert simulation2.v(c1) == approx(testdata['v(C1)'], 1e-03)
            assert simulation2.i(c1) == approx(testdata['i(C1)'], 1e-03)
            assert simulation2.v(r_load) == approx(testdata['v(R_load)'], 1e-03)
            assert simulation2.i(r_load) == approx(testdata['i(R_load)'], 1e-03)
