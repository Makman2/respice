from pytest import approx

from respice.analysis import Circuit
from respice.components import R, L, C, VoltageSourceDC, SwitchRectangular, ComplementarySwitch



def test_boost_converter_steadystate(tstdata):
    f = 1000

    src = VoltageSourceDC(10)
    switch1 = SwitchRectangular(frequency=f, duty=0.6)
    switch2 = ComplementarySwitch(switch1)
    load = R(100)
    c1 = C(5e-5)
    l1 = L(0.05)

    circuit = Circuit()
    circuit.add(src, 0, 1)
    circuit.add(l1, 1, 2)
    circuit.add(switch2, 2, 3)
    circuit.add(c1, 0, 3)
    circuit.add(load, 0, 3)
    circuit.add(switch1, 0, 2)

    simulation = circuit.steadystate(1 / f, 10)
    simulation.wait()

    testdata = tstdata.load('steadystate.csv')

    assert simulation.v(src) == approx(testdata['v(E)'])
    assert simulation.i(src) == approx(testdata['i(E)'])
    assert simulation.v(c1) == approx(testdata['v(C1)'])
    assert simulation.i(c1) == approx(testdata['i(C1)'])
    assert simulation.v(l1) == approx(testdata['v(L1)'])
    assert simulation.i(l1) == approx(testdata['i(L1)'])
    assert simulation.v(load) == approx(testdata['v(load)'])
    assert simulation.i(load) == approx(testdata['i(load)'])
    assert simulation.v(switch1) == approx(testdata['v(S1)'])
    assert simulation.i(switch1) == approx(testdata['i(S1)'])
    assert simulation.v(switch2) == approx(testdata['v(S2)'])
    assert simulation.i(switch2) == approx(testdata['i(S2)'])
