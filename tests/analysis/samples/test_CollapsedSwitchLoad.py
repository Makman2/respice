from pytest import approx

from respice.analysis import Circuit
from respice.components import R, VoltageSourceDC, SwitchRectangular, ComplementarySwitch


# Addresses https://gitlab.com/Makman2/respice/-/issues/84.


def test_collapsed_switch_load(tstdata):
    src = VoltageSourceDC(10)
    r1 = R(100)
    r2 = R(100)
    s1 = SwitchRectangular(frequency=2)
    s2 = ComplementarySwitch(s1)

    circuit = Circuit()
    circuit.add(src, 0, 1)
    circuit.add(r1, 0, 1)
    circuit.add(s1, 1, 2)
    circuit.add(s2, 0, 2)
    circuit.add(r2, 0, 2)

    simulation = circuit.simulate(0, 1, 2)
    simulation.wait()

    testdata = tstdata.load('data.csv')

    assert simulation.v(src) == approx(testdata['v(E)'])
    assert simulation.i(src) == approx(testdata['i(E)'])
    assert simulation.v(r1) == approx(testdata['v(R1)'])
    assert simulation.i(r1) == approx(testdata['i(R1)'])
    assert simulation.v(r2) == approx(testdata['v(R2)'])
    assert simulation.i(r2) == approx(testdata['i(R2)'])
    assert simulation.v(s1) == approx(testdata['v(S1)'])
    assert simulation.i(s1) == approx(testdata['i(S1)'])
    assert simulation.v(s2) == approx(testdata['v(S2)'])
    assert simulation.i(s2) == approx(testdata['i(S2)'])
