from pytest import approx

from respice.analysis import Circuit
from respice.components import LinearizedShockleyDiode, VoltageSourceHalfWaveAC
from respice.components import R, C


def test_boost_converter(tstdata):
    E = 100
    f = 60

    src = VoltageSourceHalfWaveAC(frequency=2 * f, amplitude=E)
    d1 = LinearizedShockleyDiode()
    r1 = R(0.1)
    r2 = R(100)
    c1 = C(0.0002)

    src.name = 'E'
    d1.name = 'D1'
    r1.name = 'R1'
    r2.name = 'R2'
    c1.name = 'C1'

    circuit = Circuit()
    circuit.add(src, 0, 1)
    circuit.add(d1, 2, 1)
    circuit.add(r1, 3, 2)
    circuit.add(c1, 0, 3)
    circuit.add(r2, 0, 3)

    simulation = circuit.simulate(0, 2 / f, 1000)
    simulation.wait()

    testdata = tstdata.load('data.csv')

    assert simulation.v(src) == approx(testdata['v(E)'])
    assert simulation.i(src) == approx(testdata['i(E)'])
    assert simulation.v(d1) == approx(testdata['v(D1)'])
    assert simulation.i(d1) == approx(testdata['i(D1)'])
    assert simulation.v(c1) == approx(testdata['v(C1)'])
    assert simulation.i(c1) == approx(testdata['i(C1)'])
    assert simulation.v(r1) == approx(testdata['v(R1)'])
    assert simulation.i(r1) == approx(testdata['i(R1)'])
