from pytest import approx

from respice.analysis import Circuit
from respice.components import VoltageSourceAC, R, L, C


def test_dc_oscillator(tstdata):
    r = R(10)
    l = L(1e-3)
    c = C(1e-6, 10)

    circuit = Circuit()
    circuit.add(r, 0, 1)
    circuit.add(l, 1, 2)
    circuit.add(c, 2, 0)

    simulation = circuit.simulate(0, 0.0007, 100)
    simulation.wait()

    testdata = tstdata.load('oscillator.csv')

    assert simulation.v(r) == approx(testdata['v(R)'])
    assert simulation.i(r) == approx(testdata['i'])

    assert simulation.v(l) == approx(testdata['v(L)'])
    assert simulation.i(l) == approx(testdata['i'])

    assert simulation.v(c) == approx(testdata['v(C)'])
    assert simulation.i(c) == approx(testdata['i'])


def test_ac_filter(tstdata):
    src = VoltageSourceAC(amplitude=10.0, frequency=1000)
    r = R(50)
    l = L(1e-2, 2)
    c = C(1e-5)
    r_load = R(1000)

    circuit = Circuit()
    circuit.add(src, 0, 1)
    circuit.add(l, 1, 2)
    circuit.add(r, 2, 0)
    circuit.add(c, 2, 0)
    circuit.add(r_load, 2, 0)

    simulation = circuit.simulate(0, 0.005, 100)
    simulation.wait()

    testdata = tstdata.load('filter.csv')

    assert simulation.v(src) == approx(testdata['v(E)'])
    assert simulation.i(src) == approx(testdata['i(E)'])

    assert simulation.v(r) == approx(testdata['v(R)'])
    assert simulation.i(r) == approx(testdata['i(R)'])

    assert simulation.v(l) == approx(testdata['v(L)'])
    assert simulation.i(l) == approx(testdata['i(L)'])

    assert simulation.v(c) == approx(testdata['v(C)'])
    assert simulation.i(c) == approx(testdata['i(C)'])

    assert simulation.v(r_load) == approx(testdata['v(RL)'])
    assert simulation.i(r_load) == approx(testdata['i(RL)'])

    # Do some additional consistency tests.

    assert (
        [i_r + i_r_load + i_c
         for i_r, i_r_load, i_c in zip(testdata['i(R)'], testdata['i(RL)'], testdata['i(C)'])] ==
        approx(testdata['i(E)']))
