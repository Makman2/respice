from pytest import approx

from respice.analysis import Circuit
from respice.components import L, C


def test_lc_oscillator(tstdata):
    l = L(1e-4)
    c = C(1e-6, 5)

    circuit = Circuit()
    circuit.add(l, 0, 1)
    circuit.add(c, 1, 0)

    simulation = circuit.simulate(0, 0.0002, 200)
    simulation.wait()

    testdata = tstdata.load('oscillator.csv')

    assert simulation.v(l) == approx(testdata['v(L)'])
    assert simulation.i(l) == approx(testdata['i(L)'])

    assert simulation.v(c) == approx(-testdata['v(L)'])
    assert simulation.i(c) == approx(testdata['i(L)'])
