from math import pi, sin

import numpy as np
from pytest import approx

from respice.analysis import Circuit
from respice.components import VoltageSourceAC, VoltageSourceDC, R


def test_dc_single_resistance():
    src = VoltageSourceDC(2.0)
    r = R(100)

    circuit = Circuit()
    circuit.add(src, 0, 1)
    circuit.add(r, 1, 0)

    simulation = circuit.simulate(0, 1, 100)
    simulation.wait()

    assert simulation.v(src) == approx([2.0] * 100)
    assert simulation.i(src) == approx([-0.02] * 100)
    assert simulation.v(r) == approx([-2.0] * 100)
    assert simulation.i(r) == approx(simulation.i(src))


def test_dc_multi_resistance():
    # Series with parallel connection.

    src = VoltageSourceDC(10.0)
    r1 = R(20)
    r2 = R(40)
    r3 = R(60)

    circuit = Circuit()
    circuit.add(src, 0, 1)
    circuit.add(r1, 1, 2)
    circuit.add(r2, 2, 0)
    circuit.add(r3, 2, 0)

    simulation = circuit.simulate(0, 1, 100)
    simulation.wait()

    assert simulation.v(src) == approx([10.0] * 100)
    assert simulation.i(src) == approx([-5/22] * 100)

    assert simulation.v(r1) == approx([-50/11] * 100)
    assert simulation.i(r1) == approx(simulation.i(src))

    assert simulation.v(r2) == approx([-60/11] * 100)
    assert simulation.i(r2) == approx([-3/22] * 100)

    assert simulation.v(r3) == approx(simulation.v(r2))
    assert simulation.i(r3) == approx([-1/11] * 100)


def test_ac_single_resistance():
    src = VoltageSourceAC(amplitude=2.0, frequency=100.0)
    r = R(100)

    circuit = Circuit()
    circuit.add(src, 0, 1)
    circuit.add(r, 1, 0)

    simulation = circuit.simulate(0, 0.1, 100)  # Simulate 10 periods.
    simulation.wait()

    assert simulation.v(src) == approx([2.0 * sin(x) for x in np.linspace(0, 10 * 2*pi, 100)])
    assert simulation.i(src) == approx([-0.02 * sin(x) for x in np.linspace(0, 10 * 2*pi, 100)])
    assert simulation.v(r) == approx([-x for x in simulation.v(src)])
    assert simulation.i(r) == approx(simulation.i(src))


def test_ac_multi_resistance():
    # Series with parallel connection.

    src = VoltageSourceAC(amplitude=5.0, frequency=100.0)
    r1 = R(20)
    r2 = R(40)
    r3 = R(60)

    circuit = Circuit()
    circuit.add(src, 0, 1)
    circuit.add(r1, 1, 2)
    circuit.add(r2, 2, 0)
    circuit.add(r3, 2, 0)

    simulation = circuit.simulate(0, 0.1, 100)
    simulation.wait()

    assert simulation.v(src) == approx([5.0 * sin(x) for x in np.linspace(0, 10 * 2*pi, 100)])
    assert simulation.i(src) == approx([-5/44 * sin(x) for x in np.linspace(0, 10 * 2*pi, 100)])

    assert simulation.v(r1) == approx([-25/11 * sin(x) for x in np.linspace(0, 10 * 2*pi, 100)])
    assert simulation.i(r1) == approx(simulation.i(src))

    assert simulation.v(r2) == approx([-30/11 * sin(x) for x in np.linspace(0, 10 * 2*pi, 100)])
    assert simulation.i(r2) == approx([-3/44 * sin(x) for x in np.linspace(0, 10 * 2*pi, 100)])

    assert simulation.v(r3) == approx(simulation.v(r2))
    assert simulation.i(r3) == approx([-1/22 * sin(x) for x in np.linspace(0, 10 * 2*pi, 100)])
