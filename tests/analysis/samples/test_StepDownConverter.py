from pytest import approx

from respice.analysis import Circuit
from respice.components import R, L, C
from respice.components import VoltageSourceRectangular


def test_converter(tstdata):
    v = 0.1
    v_src = VoltageSourceRectangular(amplitude=v / 2, frequency=900.0, offset=v / 2)
    r_load = R(30)
    c1 = C(1e-6)
    l1 = L(80e-3)

    converter = Circuit()
    converter.add(v_src, 0, 1)
    converter.add(l1, 1, 2)
    converter.add(c1, 0, 2)
    converter.add(r_load, 0, 2)

    simulation = converter.simulate(0, 0.01, 200)
    simulation.wait()

    testdata = tstdata.load('converter.csv')

    assert simulation.v(v_src) == approx(testdata['v(E)'])
    assert simulation.i(v_src) == approx(testdata['i(E)'])

    assert simulation.v(l1) == approx(testdata['v(L1)'])
    assert simulation.i(l1) == approx(testdata['i(L1)'])
    assert simulation.i(l1) == approx(simulation.i(v_src))

    assert simulation.v(r_load) == approx(testdata['v(R_load)'])
    assert simulation.i(r_load) == approx(testdata['i(R_load)'])

    assert simulation.v(c1) == approx(testdata['v(C1)'])
    assert simulation.v(c1) == approx(simulation.v(r_load))
    assert simulation.i(c1) == approx(testdata['i(C1)'])
