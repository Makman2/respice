from pytest import approx

from respice.analysis import Circuit
from respice.components import R, C, VoltageSourceDC, SwitchRectangular


def test_switched_voltage_source(tstdata):
    src = VoltageSourceDC(10)
    switch = SwitchRectangular(frequency=100.0)
    c1 = C(4e-5)
    r1 = R(10)
    r2 = R(50)

    circuit = Circuit()
    circuit.add(src, 0, 1)
    circuit.add(r1, 1, 2)
    circuit.add(switch, 2, 3)
    circuit.add(c1, 0, 3)
    circuit.add(r2, 0, 3)

    simulation = circuit.simulate(0, 0.03, 100)
    simulation.wait()

    testdata = tstdata.load('data.csv')

    assert simulation.v(src) == approx(testdata['v(E)'])
    assert simulation.i(src) == approx(testdata['i(E)'])
    assert simulation.v(c1) == approx(testdata['v(C1)'])
    assert simulation.i(c1) == approx(testdata['i(C1)'])
    assert simulation.v(r1) == approx(testdata['v(R1)'])
    assert simulation.i(r1) == approx(testdata['i(R1)'])
    assert simulation.v(r2) == approx(testdata['v(R2)'])
    assert simulation.i(r2) == approx(testdata['i(R2)'])
    assert simulation.v(switch) == approx(testdata['v(switch)'])
    assert simulation.i(switch) == approx(testdata['i(switch)'])
