import pytest
from pytest import approx

from respice.components import R, VoltageSourceDC
from respice.utils import create_series_connected_circuit


@pytest.mark.slow
def test_resistance_series_connection():
    src = VoltageSourceDC(10.0)
    rs1 = [R(10) for _ in range(500)]
    rs2 = [R(30) for _ in range(500)]

    circuit = create_series_connected_circuit(src, *rs1, *rs2)
    simulation = circuit.simulate(0, 0.1, 10)
    simulation.wait()

    i = [-0.0005] * 10

    assert simulation.v(src) == approx([10.0] * 10)
    assert simulation.i(src) == approx(i)

    for r in rs1:
        assert simulation.v(r) == approx([-0.005] * 10)
        assert simulation.i(r) == approx(i)

    for r in rs2:
        assert simulation.v(r) == approx([-0.015] * 10)
        assert simulation.i(r) == approx(i)
