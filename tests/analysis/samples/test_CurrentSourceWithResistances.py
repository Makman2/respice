from math import pi, sin

import numpy as np
from pytest import approx

from respice.analysis import Circuit
from respice.components import CurrentSourceAC, CurrentSourceDC, R


def test_dc_single_resistance():
    src = CurrentSourceDC(2.0)
    r = R(15)

    circuit = Circuit()
    circuit.add(src, 0, 1)
    circuit.add(r, 1, 0)

    simulation = circuit.simulate(0, 1, 100)
    simulation.wait()

    assert simulation.v(src) == approx([-30.0] * 100)
    assert simulation.i(src) == approx([2.0] * 100)
    assert simulation.v(r) == approx([30.0] * 100)
    assert simulation.i(r) == approx(simulation.i(src))


def test_dc_multi_resistance():
    # Series with parallel connection.

    src = CurrentSourceDC(2.0)
    r1 = R(20)
    r2 = R(40)
    r3 = R(60)

    circuit = Circuit()
    circuit.add(src, 0, 1)
    circuit.add(r1, 1, 2)
    circuit.add(r2, 2, 0)
    circuit.add(r3, 2, 0)

    simulation = circuit.simulate(0, 1, 100)
    simulation.wait()

    assert simulation.v(src) == approx([-88.0] * 100)
    assert simulation.i(src) == approx([2.0] * 100)

    assert simulation.v(r1) == approx([40.0] * 100)
    assert simulation.i(r1) == approx(simulation.i(src))

    assert simulation.v(r2) == approx([48.0] * 100)
    assert simulation.i(r2) == approx([1.2] * 100)

    assert simulation.v(r3) == approx(simulation.v(r2))
    assert simulation.i(r3) == approx([0.8] * 100)


def test_ac_single_resistance():
    src = CurrentSourceAC(amplitude=2.0, frequency=100.0)
    r = R(100)

    circuit = Circuit()
    circuit.add(src, 0, 1)
    circuit.add(r, 1, 0)

    simulation = circuit.simulate(0, 0.1, 100)  # Simulate 10 periods.
    simulation.wait()

    assert simulation.v(src) == approx([-200.0 * sin(x) for x in np.linspace(0, 10 * 2*pi, 100)])
    assert simulation.i(src) == approx([2.0 * sin(x) for x in np.linspace(0, 10 * 2*pi, 100)])
    assert simulation.i(r) == approx(simulation.i(src))
    assert simulation.v(r) == approx([-x for x in simulation.v(src)])
