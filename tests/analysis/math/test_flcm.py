from respice.math import lcm, flcm


def test_single_number():
    assert flcm(0) == 0
    assert flcm(1) == 1
    assert flcm(222) == 222


def test_commutative():
    assert flcm(3, 1) == flcm(3, 1)
    assert flcm(7, 8, 9, 10) == flcm(10, 7, 9, 8)


def test_integers():
    # Integers should work exactly the same as the standard lcm.
    assert flcm(1, 3) == lcm(1, 3)
    assert flcm(1, 3, 5) == lcm(1, 3, 5)
    assert flcm(0, 4, 4, 4) == lcm(0, 4, 4, 4)
    assert flcm(4, 6, 8) == lcm(4, 6, 8)
    assert flcm(1000, 1002, 1088) == lcm(1000, 1002, 1088)


def test_floats():
    assert flcm(0.1, 0.2) == 0.2
    assert flcm(0.1, 0.5) == 0.5
    assert flcm(0.3, 0.9) == 0.9
    assert flcm(0.2, 0.4, 0.6) == 1.2
    assert flcm(0.4, 0.6, 0.8) == 2.4
    assert flcm(0.0004, 0.0006, 0.0008) == 0.0024


def test_approximate_floats():
    assert flcm(0.3, 0.70001, tol=1e-3) == 2.1
    assert flcm(0.31, 0.70001, tol=1e-3) == 21.7


def test_ignore_zeros():
    assert flcm(0, 0, 0, 2, 0, 4, 0, 0, 6, 0, 0, 0, ignore_zeros=True) == 12.0
    assert flcm(1e-30, 4, 7, 1e-15, ignore_zeros=True) == 28.0
