from respice.math import lcm


def test_single_parameter():
    assert lcm(0) == 0
    assert lcm(1) == 1
    assert lcm(222) == 222


def test_commutative():
    assert lcm(3, 1) == lcm(3, 1)
    assert lcm(7, 8, 9, 10) == lcm(10, 7, 9, 8)


def test():
    assert lcm(1, 3) == 3
    assert lcm(1, 3, 5) == 15
    assert lcm(0, 4, 4, 4) == 0
    assert lcm(2, 4, 6) == 12
    assert lcm(4, 6, 8) == 24
    assert lcm(1000, 1500, 100) == 3000
