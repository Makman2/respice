import numpy as np
from math import sin, pi

import pytest
from pytest import approx

from respice.analysis import Circuit
from respice.components import VoltageSourceRectangular, L, C
from respice.components.Motor import Motor


@pytest.mark.slow
def test_event_system(tstdata):
    E = 100
    f_converter = 100e3
    f_torque = 10

    src = VoltageSourceRectangular(amplitude=E / 2, frequency=f_converter, offset=E / 2, duty=0.3, phase=0.01)
    l1 = L(420e-6)
    c1 = C(38e-6)
    motor = Motor(
        R=0.425,
        L=0.00378,
        KT=0.466,
        KE=0.466,
        J=0.02255,
        D=0.001,
        TL=lambda t: 5 + np.cbrt(sin(2 * pi * f_torque * t)),
    )

    circuit = Circuit()
    circuit.add(src, 0, 1)
    circuit.add(motor, 0, 'out')
    circuit.add(c1, 0, 'out')
    circuit.add(l1, 'out', 1)

    simulation = circuit.multirate_steadystate(
        0,
        1 / f_converter,
        1 / f_torque,
        fast_steps=2,
        # Values out of range [0, 1] are filtered out automatically. This is tested too.
        subdivisions=[-0.1, 0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.1, 0.2, 0.3, 0.4, 0.45, 0.46, 0.47, 0.48, 0.49, 0.5,
                      0.51, 0.52, 0.53, 0.54, 0.55, 0.6, 0.7, 0.8, 0.9, 0.95, 0.96, 0.97, 0.98, 0.99, 1, 1.1],
    )
    simulation.wait()

    testdata = tstdata.load('data.csv')

    assert simulation.t() == approx(testdata['t'])
    assert simulation.v(src) == approx(testdata['v(E)'])
    assert simulation.i(src) == approx(testdata['i(E)'])
    assert simulation.v(motor) == approx(testdata['v(motor)'])
    assert simulation.i(motor) == approx(testdata['i(motor)'])

    motorstates = np.transpose(simulation.s(motor))
    assert motorstates[-1] == approx(testdata['state(motor)[3]'])
