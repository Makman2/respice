import pytest
from pytest import approx

from respice.components import R, L, C, VoltageSourceRectangular
from respice.utils import create_series_connected_circuit


@pytest.mark.slow
def test_multirate_rlc_efm_simulation(tstdata):
    f_slow = 50
    f_fast = 10000

    v_src_slow = VoltageSourceRectangular(amplitude=10, frequency=f_slow)
    v_src_fast = VoltageSourceRectangular(amplitude=1, frequency=f_fast)
    r1 = R(30)
    c1 = C(1e-6)
    l1 = L(80e-3)

    circuit = create_series_connected_circuit(v_src_slow, v_src_fast, l1, c1, r1)

    simulation = circuit.simulate_efm(0, 1 / f_fast, 4, [4] * 30)
    simulation.wait()

    testdata = tstdata.load('data.csv')

    assert simulation.t() == approx(testdata['t'])
    assert simulation.v(v_src_slow) == approx(testdata['v(E1)'])
    assert simulation.i(v_src_slow) == approx(testdata['i(E1)'])
    assert simulation.v(v_src_fast) == approx(testdata['v(E2)'])
    assert simulation.i(v_src_fast) == approx(testdata['i(E2)'])
    assert simulation.v(r1) == approx(testdata['v(R1)'])
    assert simulation.i(r1) == approx(testdata['i(R1)'])
    assert simulation.v(c1) == approx(testdata['v(C1)'])
    assert simulation.i(c1) == approx(testdata['i(C1)'])
    assert simulation.v(l1) == approx(testdata['v(L1)'])
    assert simulation.i(l1) == approx(testdata['i(L1)'])
