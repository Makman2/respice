import numpy as np
import pytest
from pytest import approx

from respice.math.linalg import TaylorPolynomial

import numpy.polynomial.polynomial as poly


@pytest.mark.parametrize(
    'p',
    [
        [0],
        [1],
        [1, 2, 3],
        [-5, 7, 11, 13],
    ]
)
def test_polynome(subtests, p):
    f = poly.Polynomial(p)
    dps = [poly.Polynomial(poly.polyder(f.coef, m)) for m in range(1, len(p))]

    apts = np.linspace(-10, 10, 5)
    xs = np.linspace(-10, 10, 10)

    for a in apts:
        with subtests.test(msg='check polynomial equivalence', a=a):
            taylor = TaylorPolynomial([f(a)] + [dp(a) for dp in dps], a)
            assert taylor(xs) == approx(f(xs))


def test_exp():
    def f(x, m=0):
        return 0.5**m * np.exp(0.5 * x)

    def coeff(d, a):
        return [f(a, i) for i in range(d)]

    tol = 0.01
    xs = np.linspace(-0.6, 0.8, 10)

    taylor = TaylorPolynomial(coeff(3, 0), 0)
    assert taylor(xs) == approx(f(xs), rel=tol)
