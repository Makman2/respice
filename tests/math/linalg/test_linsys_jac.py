import numpy as np

from respice.math.linalg import check_jacobian, linsys_jac


def test_linsys_jac(subtests):
    def ss(y, t):
        # This sample has no real meaning, it is just a very bloated equation.
        A = np.array([
            [-50 * y[0] * y[1], -20 * y[0] ** 2 + 5],
            [2 * y[1] - 3, -0.002 * y[1]],
        ])
        b = np.array([
            y[0] * 100 + np.sin(y[1]),
            -y[1] * np.sin(20 * np.pi * t),
        ])
        return A, b

    def ss_jac(y, t):
        JA = np.array([
            [
                [-50 * y[1], -40 * y[0]],
                [0, 0],
            ],
            [
                [-50 * y[0], 0],
                [2, -0.002],
            ],
        ])
        Jb = np.array([
            [100, np.cos(y[1])],
            [0, -np.sin(20 * np.pi * t)],
        ])

        return JA, Jb

    def f(y, t):
        A, b = ss(y, t)
        return np.linalg.solve(A, b)

    def df(y, t):
        fx = f(y, t)
        A, b = ss(y, t)
        dA, db = ss_jac(y, t)
        return linsys_jac(fx, A, dA, db)

    ys = [[0, 0], [2, 1], [-1, 2], [10, -10], [2, 2], [2, 3], [1, 1], [5, 1]]
    t = 0.01
    maxerr = 1e-4

    for y in ys:
        with subtests.test(msg='check jacobian', y=y):
            assert all(err < maxerr for err in check_jacobian(f, df, np.array(y), t))
