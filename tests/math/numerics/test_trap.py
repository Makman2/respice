import numpy as np
from pytest import approx

from respice.itertools import pairwise
from respice.math.linalg import check_jacobian
from respice.math.numerics import trap, trap_jac


def simulate_system(t1, t2, x0, steps, ss):
    trace = [x0]

    t = np.linspace(t1, t2, steps)
    x = x0
    for tt1, tt2 in pairwise(t):
        x = trap(x, tt2 - tt1, *ss(tt1), *ss(tt2))
        trace.append(x)

    return trace


def test_sample_system(tstdata):
    def ss(t):
        A = np.array([
            [-1e2, -1e2],
            [1e6, 0],
        ])
        b = np.array([
            np.sin(2 * np.pi * 1e3 * t),
            0,
        ])
        return A, b

    x0 = np.array([0, 0])
    simulation = simulate_system(0, 0.002, x0, 50, ss)
    simulation_x1, simulation_x2 = zip(*simulation)

    testdata = tstdata.load('data.csv')

    assert simulation_x1 == approx(testdata['x1'])
    assert simulation_x2 == approx(testdata['x2'])


def test_trap_jac(subtests):
    def ss(y, t):
        # This sample has no real meaning, it is just a very bloated equation.
        A = np.array([
            [-50 * y[0] * y[1], -20 * y[0] ** 2 + 5],
            [2 * y[1] - 3, -0.002 * y[1]],
        ])
        b = np.array([
            y[0] * 100 + np.sin(y[1]),
            -y[1] * np.sin(20 * np.pi * t),
        ])
        return A, b

    def ss_jac(y, t):
        JA = np.array([
            [
                [-50 * y[1], -40 * y[0]],
                [0, 0],
            ],
            [
                [-50 * y[0], 0],
                [2, -0.002],
            ],
        ])
        Jb = np.array([
            [100, np.cos(y[1])],
            [0, -np.sin(20 * np.pi * t)],
        ])
        return JA, Jb

    def ss_trap(y2, y1, x1, t1, t2):
        h = t2 - t1
        A1, b1 = ss(y1, t1)
        A2, b2 = ss(y2, t2)
        return trap(x1, h, A1, b1, A2, b2)

    def ss_trap_jac(y2, y1, x1, t1, t2):
        h = t2 - t1
        x2 = ss_trap(y2, y1, x1, t1, t2)
        A2, b2 = ss(y2, t2)
        JA2, Jb2 = ss_jac(y2, t2)
        return trap_jac(x2, h, A2, JA2, Jb2)

    xs = [[0, 0], [2, 1], [-1, 2], [10, -10]]
    y2s = [[0, 0], [2, 1], [-1, 2], [10, -10]]
    ts = [(0, 0.1), (0.1, 0.2), (0.44, 0.45)]
    y1 = np.array([1, 1])

    for x in xs:
        for t in ts:
            for y2 in y2s:
                with subtests.test(msg='check jacobian', y2=y2, y1=y1, x=x, t=t):
                    t1, t2 = t
                    assert all(err < 1e-6 for err in check_jacobian(ss_trap,
                                                                    ss_trap_jac,
                                                                    np.array(y2),
                                                                    np.array(y1),
                                                                    np.array(x),
                                                                    t1,
                                                                    t2))
