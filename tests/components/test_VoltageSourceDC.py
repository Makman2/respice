from respice.components import VoltageSourceDC


def test():
    src = VoltageSourceDC(10.0)

    assert src.get_voltage(0, 0, 0.001) == 10.0
    assert src.get_voltage(1, 0, 0.01) == 10.0
    assert src.get_voltage(10, 0, 0.1) == 10.0
    assert src.get_voltage(100, 0, 1) == 10.0

    # Test time invariance.
    assert src.get_voltage(0, 1, 1.001) == 10.0

    # State updates have no effect.
    src.update(100.0, 0, 0.001)

    assert src.get_voltage(0, 0, 0.001) == 10.0
    assert src.get_voltage(1, 0, 0.01) == 10.0
    assert src.get_voltage(10, 0, 0.1) == 10.0
    assert src.get_voltage(100, 0, 1) == 10.0


def test_jacobian():
    src = VoltageSourceDC(30.0)
    assert src.get_jacobian(55, 0, 0.021) == 0.0
