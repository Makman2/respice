import numpy as np
from pytest import approx

from respice.components import SwitchPWM


def simulate(switch: SwitchPWM, dt, steps):
    data = []
    for i in range(steps):
        t2 = (i + 1) * dt

        data.append(switch.switch_state(t2))

    return data


def test_simple():
    switch = SwitchPWM(2 * np.pi * np.array([0.1, 0.3, 0.8]), frequency=10.0)

    assert simulate(switch, 0.005, 60) == ([False] * 2 + [True] * 4 + [False] * 10 + [True] * 3 +
                                           [False] * 3 + [True] * 4 + [False] * 10 + [True] * 3 +
                                           [False] * 3 + [True] * 4 + [False] * 9 + [True] * 4 +
                                           [False])

    switch2 = SwitchPWM(2 * np.pi * np.array([0.1, 0.3, 0.8]), initial_state=True, frequency=10.0)

    assert simulate(switch2, 0.005, 60) == [not s for s in simulate(switch, 0.005, 60)]


def test_events():
    switch = SwitchPWM(2 * np.pi * np.array([0.1, 0.3, 0.8]), frequency=10.0)

    # The parameter is actually not used internally at all, still create a proper stub.
    def stub_interpolator(_):
        return np.zeros(1)

    assert switch.next_event(stub_interpolator, 0, 1) == approx(0)
    assert switch.next_event(stub_interpolator, 0, 0.1) == approx(0)
    assert switch.next_event(stub_interpolator, 0, 0.011) == approx(0)
    assert switch.next_event(stub_interpolator, 0.015, 0.1) == approx(0.03)
    assert switch.next_event(stub_interpolator, 0.015, 0.035) == approx(0.03)
    assert switch.next_event(stub_interpolator, 1, 1.2) == approx(1)
    assert switch.next_event(stub_interpolator, 5.015, 5.035) == approx(5.03)

    assert switch.next_event(stub_interpolator, 0.015, 0.017) is None
    assert switch.next_event(stub_interpolator, 1, 1) == approx(1)


def test_events_equal_start_and_end_pattern_levels():
    # No events shall now be triggered between start and end.

    switch = SwitchPWM(2 * np.pi * np.array([0.1, 0.3, 0.8, 0.9]), frequency=10.0)

    # The parameter is actually not used internally at all, still create a proper stub.
    def stub_interpolator(_):
        return np.zeros(1)

    assert switch.next_event(stub_interpolator, 0, 1) == approx(0.01)
    assert switch.next_event(stub_interpolator, 0, 0.1) == approx(0.01)
    assert switch.next_event(stub_interpolator, 0, 0.011) == approx(0.01)
    assert switch.next_event(stub_interpolator, 0.015, 0.1) == approx(0.03)
    assert switch.next_event(stub_interpolator, 0.015, 0.035) == approx(0.03)
    assert switch.next_event(stub_interpolator, 1, 1.2) == approx(1.01)
    assert switch.next_event(stub_interpolator, 5.015, 5.035) == approx(5.03)

    assert switch.next_event(stub_interpolator, 0.015, 0.017) is None
    assert switch.next_event(stub_interpolator, 1, 1) is None
