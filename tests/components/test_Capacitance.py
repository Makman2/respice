import numpy as np
from pytest import approx

from respice.components import C, Capacitance


def test_class_aliases():
    assert C is Capacitance


def test_some_values_without_initial_charge():
    c = C(100e-6)

    assert c.get_voltage(10.0, 0, 0.001) == 50.0
    assert c.get_voltage(1.0, 0, 0.01) == 50.0
    assert c.get_voltage(20.0, 0, 0.5) == 50000.0
    assert c.get_voltage(-4.0, 0, 0.002) == -40.0

    # Test time invariance.
    assert c.get_voltage(10.0, 2, 2.001) == approx(50.0)
    assert c.get_voltage(1.0, -1, -0.99) == approx(50.0)
    assert c.get_voltage(20.0, 0.1, 0.6) == approx(50000.0)
    assert c.get_voltage(-4.0, 0.001, 0.003) == approx(-40.0)

    c = C(0.02)

    assert c.get_voltage(-10.0, 0, 0.001) == -0.25
    assert c.get_voltage(-1.0, 0, 0.01) == -0.25
    assert c.get_voltage(-20.0, 0, 0.5) == -250.0
    assert c.get_voltage(4.0, 0, 0.002) == 0.2

    # Test time invariance.
    assert c.get_voltage(-10.0, 1, 1.001) == approx(-0.25)
    assert c.get_voltage(-1.0, -3, -2.99) == approx(-0.25)
    assert c.get_voltage(-20.0, 0.2, 0.7) == approx(-250.0)
    assert c.get_voltage(4.0, 0.002, 0.004) == approx(0.2)


def test_some_values_with_initial_charge():
    c = C(100e-6, 1.0)

    assert c.get_voltage(10.0, 0, 0.001) == 51.0
    assert c.get_voltage(1.0, 0, 0.01) == 51.0
    assert c.get_voltage(20.0, 0, 0.5) == 50001.0
    assert c.get_voltage(-4.0, 0, 0.002) == -39.0

    # Test time invariance.
    assert c.get_voltage(10.0, 2, 2.001) == approx(51.0)
    assert c.get_voltage(1.0, -1, -0.99) == approx(51.0)
    assert c.get_voltage(20.0, 0.1, 0.6) == approx(50001.0)
    assert c.get_voltage(-4.0, 0.001, 0.003) == approx(-39.0)

    c = C(0.02, -10.0)

    assert c.get_voltage(-10.0, 0, 0.001) == -10.25
    assert c.get_voltage(-1.0, 0, 0.01) == -10.25
    assert c.get_voltage(-20.0, 0, 0.5) == -260.0
    assert c.get_voltage(4.0, 0, 0.002) == -9.8

    # Test time invariance.
    assert c.get_voltage(-10.0, 1, 1.001) == approx(-10.25)
    assert c.get_voltage(-1.0, -3, -2.99) == approx(-10.25)
    assert c.get_voltage(-20.0, 0.2, 0.7) == approx(-260.0)
    assert c.get_voltage(4.0, 0.002, 0.004) == approx(-9.8)


def test_some_values_with_initial_charge_and_current():
    c = C(100e-6, 1.0, 2.0)

    assert c.get_voltage(10.0, 0, 0.001) == 61.0
    assert c.get_voltage(1.0, 0, 0.01) == 151.0
    assert c.get_voltage(20.0, 0, 0.5) == 55001.0
    assert c.get_voltage(-4.0, 0, 0.002) == -19.0

    # Test time invariance.
    assert c.get_voltage(10.0, 2, 2.001) == approx(61.0)
    assert c.get_voltage(1.0, -1, -0.99) == approx(151.0)
    assert c.get_voltage(20.0, 0.1, 0.6) == approx(55001.0)
    assert c.get_voltage(-4.0, 0.001, 0.003) == approx(-19.0)

    c = C(0.02, -10.0, -4.0)

    assert c.get_voltage(-10.0, 0, 0.001) == -10.35
    assert c.get_voltage(-1.0, 0, 0.01) == -11.25
    assert c.get_voltage(-20.0, 0, 0.5) == -310.0
    assert c.get_voltage(4.0, 0, 0.002) == -10.0

    # Test time invariance.
    assert c.get_voltage(-10.0, 1, 1.001) == approx(-10.35)
    assert c.get_voltage(-1.0, -3, -2.99) == approx(-11.25)
    assert c.get_voltage(-20.0, 0.2, 0.7) == approx(-310.0)
    assert c.get_voltage(4.0, 0.002, 0.004) == approx(-10.0)


def test_zero_current():
    c = C(1e6)

    assert c.get_voltage(0.0, 0, 1.0) == 0.0
    assert c.get_voltage(0.0, 0, 0.1) == 0.0
    assert c.get_voltage(0.0, 0, 0.001) == 0.0


def test_state():
    c = C(100e-6)

    assert c.get_voltage(0.1, 0, 0.001) == 0.5
    c.update(0.1, 0, 0.001)
    assert c.get_voltage(0.1, 0, 0.001) == 1.5
    c.update(0.1, 0, 0.001)
    assert c.get_voltage(0.1, 0, 0.001) == 2.5
    c.update(0.1, 0, 0.001)
    assert c.get_voltage(0.2, 0, 0.001) == 4.0
    c.update(0.2, 0, 0.001)
    assert c.get_voltage(0.4, 0, 0.0001) == 4.3
    c.update(0.4, 0, 0.0001)
    assert c.get_voltage(0.0, 0, 0.001) == 6.3


def test_symmetry():
    # It makes no difference whether a capacitor is connected from A to B
    # or vice versa (from considering zero initial state).
    c = C(10e-6)

    assert c.get_voltage(20.0, 0, 1.0) == -c.get_voltage(-20.0, 0, 1.0)
    assert c.get_voltage(1.0, 0, 1.0) == -c.get_voltage(-1.0, 0, 1.0)
    assert c.get_voltage(-1000.0, 0, 1.0) == -c.get_voltage(1000.0, 0, 1.0)


def test_jacobian():
    # The Jacobian is independent from momentary current.
    for i in [-10, -5, -1, 1, 100]:
        c = C(20e-6)
        assert c.get_jacobian(i, 0, 0.1) == approx(2500.0)
        assert c.get_jacobian(i, 0, 0.01) == approx(250.0)
        assert c.get_jacobian(i, 0, 0.001) == approx(25.0)
        assert c.get_jacobian(i, 0, 0.0004) == approx(10.0)

        c = C(5e-3)
        assert c.get_jacobian(i, 0, 0.1) == approx(10.0)
        assert c.get_jacobian(i, 0, 0.01) == approx(1.0)
        assert c.get_jacobian(i, 0, 0.001) == approx(0.1)
        assert c.get_jacobian(i, 0, 0.0004) == approx(0.04)

    # The Jacobian is independent from the state for capacitances.
    c = C(100e-9)

    for i in range(5):
        assert c.get_jacobian(1, 0, 0.001) == approx(5000.0)
        c.update(np.array([i]), 0, 0.001)
        assert c.get_jacobian(1, 0, 0.00005) == approx(250.0)
        c.update(np.array([i]), 0, 0.00005)


def test_state():
    assert C(1e-6).state == (0, 0)
    assert C(1e-6, 10).state == (10, 0)
    assert C(1e-6, 20, 30).state == (20, 30)

    c = C(1e-6)
    c.state = (1, 4)
    assert c.state_voltage == 1
    assert c.state_current == 4
