from math import pi

from pytest import approx

from respice.components import VoltageSourceAC


def simulate(src: VoltageSourceAC, dt, steps):
    data = []
    for i in range(steps):
        t1 = i * dt
        t2 = t1 + dt
        data.append(src.get_voltage(0.0, t1, t2))
        src.update(0.0, t1, t2)

    return data


def test_simple():
    src = VoltageSourceAC(amplitude=10.0, frequency=100.0)

    assert simulate(src, 0.001, 10) == approx([5.878, 9.511, 9.511, 5.878, 0, -5.878, -9.511, -9.511, -5.878, 0],
                                              rel=1e-4)
    assert simulate(src, 0.0001, 100) == approx([0.628, 1.253, 1.874, 2.487, 3.09, 3.681, 4.258, 4.818, 5.358, 5.878,
                                                 6.374, 6.845, 7.29, 7.705, 8.09, 8.443, 8.763, 9.048, 9.298, 9.511,
                                                 9.686, 9.823, 9.921, 9.98, 10.0, 9.98, 9.921, 9.823, 9.686, 9.511,
                                                 9.298, 9.048, 8.763, 8.443, 8.09, 7.705, 7.29, 6.845, 6.374, 5.878,
                                                 5.358, 4.818, 4.258, 3.681, 3.09, 2.487, 1.874, 1.253, 0.628, 0.0,
                                                 -0.628, -1.253, -1.874, -2.487, -3.09, -3.681, -4.258, -4.818, -5.358,
                                                 -5.878, -6.374, -6.845, -7.29, -7.705, -8.09, -8.443, -8.763, -9.048,
                                                 -9.298, -9.511, -9.686, -9.823, -9.921, -9.98, -10.0, -9.98, -9.921,
                                                 -9.823, -9.686, -9.511, -9.298, -9.048, -8.763, -8.443, -8.09, -7.705,
                                                 -7.29, -6.845, -6.374, -5.878, -5.358, -4.818, -4.258, -3.681, -3.09,
                                                 -2.487, -1.874, -1.253, -0.628, -0.0],
                                                rel=1e-3)

    src = VoltageSourceAC(amplitude=1.0, frequency=0.1)

    assert simulate(src, 1.0, 10) == approx([0.5878, 0.9511, 0.9511, 0.5878, 0, -0.5878, -0.9511, -0.9511, -0.5878, 0],
                                            rel=1e-4)


def test_phase_shift():
    # Periodicity property
    assert simulate(VoltageSourceAC(amplitude=1.0, frequency=1.0), 0.01, 100) == approx(
        simulate(VoltageSourceAC(amplitude=1.0, frequency=1.0, phase=2*pi), 0.01, 100))

    assert simulate(VoltageSourceAC(amplitude=1.0, frequency=1.0, phase=0.5*pi), 0.01, 100) == approx(
        simulate(VoltageSourceAC(amplitude=1.0, frequency=1.0, phase=2.5*pi), 0.01, 100))

    assert simulate(VoltageSourceAC(amplitude=1.0, frequency=1.0, phase=1.8 * pi), 0.01, 100) == approx(
        simulate(VoltageSourceAC(amplitude=1.0, frequency=1.0, phase=-2.2 * pi), 0.01, 100))

    # Half-wave symmetry
    assert simulate(VoltageSourceAC(amplitude=1.0, frequency=1.0), 0.01, 100) == approx(
        [-x for x in simulate(VoltageSourceAC(amplitude=1.0, frequency=1.0, phase=pi), 0.01, 100)])

    assert simulate(VoltageSourceAC(amplitude=1.0, frequency=1.0, phase=0.25*pi), 0.01, 100) == approx(
        [-x for x in simulate(VoltageSourceAC(amplitude=1.0, frequency=1.0, phase=1.25*pi), 0.01, 100)])


def test_effective_amplitude():
    src = VoltageSourceAC(amplitude=325.0, frequency=50.0)
    assert src.effective_amplitude == approx(230.0, 1e-3)

    src.effective_amplitude = 100.0
    assert src.amplitude == approx(141.42135)


def test_jacobian():
    src = VoltageSourceAC(amplitude=10.0, frequency=50.0, phase=0.2)
    assert src.get_jacobian(1000, 0, 0.01) == 0.0
