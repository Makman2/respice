import numpy as np
from pytest import approx

from respice.components import L, Inductance


def test_class_aliases():
    assert L is Inductance


def test_some_values_without_initial_charge():
    l = L(100e-6)

    assert l.get_current(10.0, 0, 0.001) == 50.0
    assert l.get_current(1.0, 0, 0.01) == 50.0
    assert l.get_current(20.0, 0, 0.5) == 50000.0
    assert l.get_current(-4.0, 0, 0.002) == -40.0

    # Test time invariance.
    assert l.get_current(10.0, 1, 1.001) == approx(50.0)
    assert l.get_current(1.0, -1, -0.99) == approx(50.0)
    assert l.get_current(20.0, 2, 2.5) == approx(50000.0)
    assert l.get_current(-4.0, 0.001, 0.003) == approx(-40.0)

    l = L(0.02)

    assert l.get_current(-10.0, 0, 0.001) == -0.25
    assert l.get_current(-1.0, 0, 0.01) == -0.25
    assert l.get_current(-20.0, 0, 0.5) == -250.0
    assert l.get_current(4.0, 0, 0.002) == 0.2

    # Test time invariance.
    assert l.get_current(-10.0, 1, 1.001) == approx(-0.25)
    assert l.get_current(-1.0, -1, -0.99) == approx(-0.25)
    assert l.get_current(-20.0, 2, 2.5) == approx(-250.0)
    assert l.get_current(4.0, 0.001, 0.003) == approx(0.2)


def test_some_values_with_initial_charge():
    l = L(100e-6, 1.0)

    assert l.get_current(10.0, 0, 0.001) == 51.0
    assert l.get_current(1.0, 0, 0.01) == 51.0
    assert l.get_current(20.0, 0, 0.5) == 50001.0
    assert l.get_current(-4.0, 0, 0.002) == -39.0

    # Test time invariance.
    assert l.get_current(10.0, 1, 1.001) == approx(51.0)
    assert l.get_current(1.0, -1, -0.99) == approx(51.0)
    assert l.get_current(20.0, 2, 2.5) == approx(50001.0)
    assert l.get_current(-4.0, 0.001, 0.003) == approx(-39.0)

    l = L(0.02, -10.0)

    assert l.get_current(-10.0, 0, 0.001) == -10.25
    assert l.get_current(-1.0, 0, 0.01) == -10.25
    assert l.get_current(-20.0, 0, 0.5) == -260.0
    assert l.get_current(4.0, 0, 0.002) == -9.8

    # Test time invariance.
    assert l.get_current(-10.0, 1, 1.001) == approx(-10.25)
    assert l.get_current(-1.0, -1, -0.99) == approx(-10.25)
    assert l.get_current(-20.0, 2, 2.5) == approx(-260.0)
    assert l.get_current(4.0, 0.001, 0.003) == approx(-9.8)


def test_some_values_with_initial_charge_and_voltage():
    l = L(100e-6, 1.0, 20.0)

    assert l.get_current(10.0, 0, 0.001) == 151.0
    assert l.get_current(1.0, 0, 0.01) == 1051.0
    assert l.get_current(20.0, 0, 0.5) == 100001.0
    assert l.get_current(-4.0, 0, 0.002) == 161.0

    # Test time invariance.
    assert l.get_current(10.0, 1, 1.001) == approx(151.0)
    assert l.get_current(1.0, -1, -0.99) == approx(1051.0)
    assert l.get_current(20.0, 2, 2.5) == approx(100001.0)
    assert l.get_current(-4.0, 0.001, 0.003) == approx(161.0)

    l = L(0.02, -10.0, -60.0)

    assert l.get_current(-10.0, 0, 0.001) == -11.75
    assert l.get_current(-1.0, 0, 0.01) == -25.25
    assert l.get_current(-20.0, 0, 0.5) == -1010.0
    assert l.get_current(4.0, 0, 0.002) == -12.8

    # Test time invariance.
    assert l.get_current(-10.0, 1, 1.001) == approx(-11.75)
    assert l.get_current(-1.0, -1, -0.99) == approx(-25.25)
    assert l.get_current(-20.0, 2, 2.5) == approx(-1010.0)
    assert l.get_current(4.0, 0.001, 0.003) == approx(-12.8)


def test_zero_voltage():
    l = L(1e-6)

    assert l.get_current(0.0, 0, 1.0) == 0.0
    assert l.get_current(0.0, 0, 0.1) == 0.0
    assert l.get_current(0.0, 0, 0.001) == 0.0


def test_zero_voltage_with_initial_charge():
    l = L(1e-6, 2.0)

    assert l.get_current(0.0, 0, 1.0) == 2.0
    assert l.get_current(0.0, 0, 0.1) == 2.0
    assert l.get_current(0.0, 0, 0.001) == 2.0


def test_state():
    l = L(100e-6)

    assert l.get_current(0.1, 0, 0.001) == 0.5
    l.update(0.1, 0, 0.001)
    assert l.get_current(0.1, 0, 0.001) == 1.5
    l.update(0.1, 0, 0.001)
    assert l.get_current(0.1, 0, 0.001) == 2.5
    l.update(0.1, 0, 0.001)
    assert l.get_current(0.2, 0, 0.001) == 4.0
    l.update(0.2, 0, 0.001)
    assert l.get_current(0.4, 0, 0.0001) == 4.3
    l.update(0.4, 0, 0.0001)
    assert l.get_current(0.0, 0, 0.001) == 6.3
    l.update(-0.5, 0, 0.0001)
    assert l.get_current(0.0, 0, 0.001) == 1.75


def test_symmetry():
    # It makes no difference whether an inductor is connected from A to B
    # or vice versa (from considering zero initial state).
    l = L(10e-6)

    assert l.get_current(20.0, 0, 1.0) == -l.get_current(-20.0, 0, 1.0)
    assert l.get_current(1.0, 0, 1.0) == -l.get_current(-1.0, 0, 1.0)
    assert l.get_current(-1000.0, 0, 1.0) == -l.get_current(1000.0, 0, 1.0)


def test_jacobian():
    # The Jacobian is independent from momentary voltage.
    for v in [-10, -5, -1, 1, 100]:
        l = L(0.002)
        assert l.get_jacobian(v, 0, 0.1) == approx(25.0)
        assert l.get_jacobian(v, 0, 0.01) == approx(2.5)
        assert l.get_jacobian(v, 0, 0.001) == approx(0.25)
        assert l.get_jacobian(v, 0, 0.0004) == approx(0.1)

        l = L(5e-2)
        assert l.get_jacobian(v, 0, 0.1) == approx(1.0)
        assert l.get_jacobian(v, 0, 0.01) == approx(0.1)
        assert l.get_jacobian(v, 0, 0.001) == approx(0.01)
        assert l.get_jacobian(v, 0, 0.0004) == approx(0.004)

    # The Jacobian is independent from the state for inductances.
    l = L(100e-6)

    for v in range(5):
        assert l.get_jacobian(1, 0, 0.001) == approx(5.0)
        l.update(np.array([v]), 0, 0.001)
        assert l.get_jacobian(1, 0, 0.00005) == approx(0.25)
        l.update(np.array([v]), 0, 0.00005)


def test_state():
    assert L(1e-3).state == (0, 0)
    assert L(1e-3, 10).state == (10, 0)
    assert L(1e-3, 20, 30).state == (20, 30)

    l = L(1e-6)
    l.state = (1, 4)
    assert l.state_current == 1
    assert l.state_voltage == 4
