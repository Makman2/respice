from pytest import approx

from respice.components import R, Resistance


def test_class_aliases():
    assert R is Resistance


def test_some_values():
    r = R(100)

    assert r.get_current(1, 0, 1.0) == 0.01
    assert r.get_current(100.0, 0, 1.0) == 1.0
    assert r.get_current(20.0, 0, 1.0) == 0.2
    assert r.get_current(-50.0, 0, 1.0) == -0.5

    # Test time invariance.
    assert r.get_current(1, 1.0, 2.0) == 0.01
    assert r.get_current(100.0, 1.0, 2.0) == 1.0
    assert r.get_current(20.0, 2.0, 3.0) == 0.2
    assert r.get_current(-50.0, -2.0, -1.0) == -0.5

    r = R(20)

    assert r.get_current(1, 0, 1.0) == 0.05
    assert r.get_current(100.0, 0, 1.0) == 5.0
    assert r.get_current(20.0, 0, 1.0) == 1.0
    assert r.get_current(-50.0, 0, 1.0) == -2.5


def test_zero_voltage():
    r = R(1000)

    assert r.get_current(0.0, 0, 1.0) == 0.0


def test_statelessness():
    r = R(100)

    assert r.get_current(1.0, 0, 1.0) == 0.01
    r.update(1.0, 0, 1.0)
    assert r.get_current(1.0, 0, 0.5) == 0.01
    r.update(1.0, 0, 0.5)
    assert r.get_current(1.0, 0, 0.1) == 0.01
    r.update(1.0, 0, 0.1)
    assert r.get_current(1.0, 0, 1.0) == 0.01


def test_symmetry():
    # It makes no difference whether a resistance is connected from A to B
    # or vice versa.
    r = R(40)

    assert r.get_current(20.0, 0, 1.0) == -r.get_current(-20.0, 0, 1.0)
    assert r.get_current(1.0, 0, 1.0) == -r.get_current(-1.0, 0, 1.0)
    assert r.get_current(-1000.0, 0, 1.0) == -r.get_current(1000.0, 0, 1.0)


def test_jacobian():
    # The Jacobian is independent from momentary current and time, it only depends
    # on the resistance value.
    for v in [-10, -5, -1, 1, 100]:
        for t2 in [0.1, 0.01, 0.00001]:
            r = R(100)
            assert r.get_jacobian(v, 0, t2) == approx(0.01)

            r = R(2000)
            assert r.get_jacobian(v, 0, t2) == approx(0.0005)

    # The Jacobian is independent from the state.
    r = R(5)

    for i in range(5):
        assert r.get_jacobian(1, 0, 0.001) == approx(0.2)
        r.update(i, 0, 0.001)
