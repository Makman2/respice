from respice.components import CurrentSourceDC


def test():
    src = CurrentSourceDC(10.0)

    assert src.get_current(0, 0, 0.001) == 10.0
    assert src.get_current(1, 0, 0.01) == 10.0
    assert src.get_current(10, 0, 0.1) == 10.0
    assert src.get_current(100, 0, 1) == 10.0

    # Test time invariance.
    assert src.get_current(0, 1, 1.001) == 10.0

    # State updates have no effect.
    src.update(100.0, 0, 0.001)

    assert src.get_current(0, 0, 0.001) == 10.0
    assert src.get_current(1, 0, 0.01) == 10.0
    assert src.get_current(10, 0, 0.1) == 10.0
    assert src.get_current(100, 0, 1) == 10.0

    # Test time invariance.
    assert src.get_current(0, 1, 1.001) == 10.0


def test_jacobian():
    src = CurrentSourceDC(30.0)
    assert src.get_jacobian(55, 0, 0.021) == 0.0
