from math import pi

from pytest import approx

from respice.components import VoltageSourceRectangular


def simulate(src: VoltageSourceRectangular, dt, steps):
    data = []
    for i in range(steps):
        t1 = i * dt
        t2 = t1 + dt
        data.append(src.get_voltage(0.0, t1, t2))
        src.update(0.0, t1, t2)

    return data


def test_simple():
    src = VoltageSourceRectangular(amplitude=10.0, frequency=100.0)

    # Little floating point arithmetic caused asymmetry.
    assert simulate(src, 0.001, 20) == [10.0] * 4 + [-10.0] * 5 + [10.0] * 5 + [-10.0] * 5 + [10.0]
    assert simulate(src, 0.0001, 100) == [10.0] * 49 + [-10.0] * 50 + [10.0]

    src = VoltageSourceRectangular(amplitude=1.0, frequency=0.1)

    assert simulate(src, 1.0, 10) == [1.0] * 4 + [-1.0] * 5 + [1.0]

    src = VoltageSourceRectangular(amplitude=0.5, frequency=0.1)

    assert simulate(src, 1.0, 10) == [0.5] * 4 + [-0.5] * 5 + [0.5]


def test_offset():
    src = VoltageSourceRectangular(amplitude=5.0, frequency=100.0, offset=5.0)
    assert simulate(src, 0.001, 20) == [10.0] * 4 + [0.0] * 5 + [10.0] * 5 + [0.0] * 5 + [10.0]

    src = VoltageSourceRectangular(amplitude=0.25, frequency=0.1, offset=-0.25)
    assert simulate(src, 1.0, 20) == [0.0] * 4 + [-0.5] * 5 + [0.0] * 5 + [-0.5] * 5 + [0.0]

    src = VoltageSourceRectangular(amplitude=1.0, frequency=10, offset=0.2)
    assert simulate(src, 0.005, 40) == [1.2] * 9 + [-0.8] * 10 + [1.2] * 10 + [-0.8] * 10 + [1.2]


def test_phase_shift():
    # Test via periodicity property
    assert (
        simulate(VoltageSourceRectangular(amplitude=1.0, frequency=1.0), 0.01, 100) ==
        simulate(VoltageSourceRectangular(amplitude=1.0, frequency=1.0, phase=2*pi), 0.01, 100)
    )

    assert (
        simulate(VoltageSourceRectangular(amplitude=1.0, frequency=1.0, phase=0.5 * pi), 0.01, 100) ==
        simulate(VoltageSourceRectangular(amplitude=1.0, frequency=1.0, phase=2.5 * pi), 0.01, 100)
    )

    assert (
        simulate(VoltageSourceRectangular(amplitude=1.0, frequency=1.0, phase=-1 * pi), 0.01, 100) ==
        simulate(VoltageSourceRectangular(amplitude=1.0, frequency=1.0, phase=-3 * pi), 0.01, 100)
    )

    # Test via half-wave symmetry

    assert (
        simulate(VoltageSourceRectangular(amplitude=1.0, frequency=1.0), 0.01, 100) ==
        [-x for x in simulate(VoltageSourceRectangular(amplitude=1.0, frequency=1.0, phase=pi), 0.01, 100)]
    )

    assert (
        simulate(VoltageSourceRectangular(amplitude=1.0, frequency=1.0, phase=0.25 * pi), 0.01, 100) ==
        [-x for x in simulate(VoltageSourceRectangular(amplitude=1.0, frequency=1.0, phase=1.25 * pi), 0.01, 100)]
    )


def test_duty_cycle():
    # Default duty cycle of 0.5
    src = VoltageSourceRectangular(amplitude=1.0, frequency=20.0)
    assert src.duty == 0.5
    assert simulate(src, 0.005, 20) == 4 * [1.0] + 5 * [-1.0] + 5 * [1.0] + 5 * [-1.0] + [1.0]

    # Duty cycle of 0 or below.
    for duty in [-100, -0.1, 0]:
        assert simulate(VoltageSourceRectangular(amplitude=1.0, frequency=20.0, duty=duty), 0.005, 20) == 20 * [-1.0]

    # Duty cycle of 1 or above.
    for duty in [1, 1.1, 100]:
        assert simulate(VoltageSourceRectangular(amplitude=1.0, frequency=20.0, duty=duty), 0.005, 20) == 20 * [1.0]

    # Duty cycles 0.25, 0.75, 0.9.
    assert (
        simulate(VoltageSourceRectangular(amplitude=1.0, frequency=20.0, duty=0.25), 0.00125, 80) ==
        9 * [1.0] + 30 * [-1.0] + 10 * [1.0] + 30 * [-1.0] + [1.0]
    )
    assert (
        simulate(VoltageSourceRectangular(amplitude=1.0, frequency=20.0, duty=0.75), 0.00125, 80) ==
        29 * [1.0] + 10 * [-1.0] + 30 * [1.0] + 10 * [-1.0] + [1.0]
    )
    assert (
        simulate(VoltageSourceRectangular(amplitude=1.0, frequency=20.0, duty=0.9), 0.001, 100) ==
        44 * [1.0] + 5 * [-1.0] + 45 * [1.0] + 5 * [-1.0] + [1.0]
    )

    # Duty cycle of 0.4 with phase shift of pi/2 = 90 deg and -pi/4 = -45 deg.
    assert (
        simulate(VoltageSourceRectangular(amplitude=1.0, frequency=20.0, duty=0.4, phase=pi/2), 0.001, 100) ==
        7 * [1.0] + 30 * [-1.0] + 20 * [1.0] + 30 * [-1.0] + 13 * [1.0]
    )
    assert (
        simulate(VoltageSourceRectangular(amplitude=1.0, frequency=20.0, duty=0.4, phase=-pi/4), 0.001, 100) ==
        6 * [-1.0] + 20 * [1.0] + 30 * [-1.0] + 20 * [1.0] + 24 * [-1.0]
    )


def test_effective_amplitude():
    src = VoltageSourceRectangular(amplitude=325.0, frequency=50.0)
    assert src.effective_amplitude == approx(325, 1e-3)

    src = VoltageSourceRectangular(amplitude=325.0, frequency=50.0, offset=100.0)
    assert src.effective_amplitude == approx(340, 1e-3)

    src = VoltageSourceRectangular(amplitude=325.0, frequency=50.0, offset=100.0, duty=0.25)
    assert src.effective_amplitude == approx(288.3, 1e-3)

    src = VoltageSourceRectangular(amplitude=325.0, frequency=50.0, offset=100.0, duty=0.75)
    assert src.effective_amplitude == approx(384.9, 1e-3)


def test_jacobian():
    src = VoltageSourceRectangular(amplitude=10.0, frequency=50.0, offset=2.5, phase=0.2)
    assert src.get_jacobian(1000, 0, 0.01) == 0.0
