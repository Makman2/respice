import numpy as np
from pytest import approx

from respice.components import VoltageSourcePWM


def simulate(src: VoltageSourcePWM, dt, steps):
    data = []
    for i in range(steps):
        t1 = i * dt
        t2 = t1 + dt

        data.append(src.get_voltage(0.0, t1, t2))
        src.update(0.0, t1, t2)

    return data


def test_simple():
    src = VoltageSourcePWM([1, 2, 3, 4], 2 * np.pi * np.array([0.1, 0.3, 0.8]), frequency=10.0)

    assert simulate(src, 0.005, 60) == ([1] * 2 + [2] * 3 + [3] * 11 + [4] * 3 +
                                        [1] * 3 + [2] * 4 + [3] * 10 + [4] * 3 +
                                        [1] * 3 + [2] * 4 + [3] * 9 + [4] * 4 +
                                        [1])


def test_events():
    src = VoltageSourcePWM([1, 2, 3, 4], 2 * np.pi * np.array([0.1, 0.3, 0.8]), frequency=10.0)

    # The parameter is actually not used internally at all, still create a proper stub.
    def stub_interpolator(_):
        return np.zeros(1)

    assert src.next_event(stub_interpolator, 0, 1) == approx(0)
    assert src.next_event(stub_interpolator, 0, 0.1) == approx(0)
    assert src.next_event(stub_interpolator, 0, 0.011) == approx(0)
    assert src.next_event(stub_interpolator, 0.015, 0.1) == approx(0.03)
    assert src.next_event(stub_interpolator, 0.015, 0.035) == approx(0.03)
    assert src.next_event(stub_interpolator, 1, 1.2) == approx(1)
    assert src.next_event(stub_interpolator, 5.015, 5.035) == approx(5.03)

    assert src.next_event(stub_interpolator, 0.015, 0.017) is None
    assert src.next_event(stub_interpolator, 1, 1) == approx(1)


def test_events_equal_start_and_end_pattern_levels():
    # No events shall now be triggered between start and end.

    src = VoltageSourcePWM([1, 2, 3, 1], 2 * np.pi * np.array([0.1, 0.3, 0.8]), frequency=10.0)

    # The parameter is actually not used internally at all, still create a proper stub.
    def stub_interpolator(_):
        return np.zeros(1)

    assert src.next_event(stub_interpolator, 0, 1) == approx(0.01)
    assert src.next_event(stub_interpolator, 0, 0.1) == approx(0.01)
    assert src.next_event(stub_interpolator, 0, 0.011) == approx(0.01)
    assert src.next_event(stub_interpolator, 0.015, 0.1) == approx(0.03)
    assert src.next_event(stub_interpolator, 0.015, 0.035) == approx(0.03)
    assert src.next_event(stub_interpolator, 1, 1.2) == approx(1.01)
    assert src.next_event(stub_interpolator, 5.015, 5.035) == approx(5.03)

    assert src.next_event(stub_interpolator, 0.015, 0.017) is None
    assert src.next_event(stub_interpolator, 1, 1) is None
