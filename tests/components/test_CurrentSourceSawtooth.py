from itertools import product
from math import pi

import numpy as np
from pytest import approx

from respice.components import CurrentSourceSawtooth


def simulate(src: CurrentSourceSawtooth, dt, steps):
    data = []
    for i in range(steps):
        t1 = i * dt
        t2 = t1 + dt
        data.append(src.get_current(0.0, t1, t2))
        src.update(0.0, t1, t2)

    return np.array(data, dtype=float)


def approx_array(lst, *args, **kwargs):
    # Leveraging pytest's ability to compare each entry using numpy arrays and make it more convenient to use here.
    return approx(np.array(lst), *args, **kwargs)


def test_simple():
    src = CurrentSourceSawtooth(amplitude=10.0, frequency=100.0)

    # Little floating point arithmetic caused asymmetry.
    assert simulate(src, 0.01000000001, 10) == approx_array([-10.0] * 10)
    assert simulate(src, 0.00100000001, 19) == approx_array([
        -8.0, -6.0, -4.0, -2.0, 0.0, 2.0, 4.0, 6.0, 8.0,
        -10.0, -8.0, -6.0, -4.0, -2.0, 0.0, 2.0, 4.0, 6.0, 8.0,
    ], abs=1e-6)

    src = CurrentSourceSawtooth(amplitude=1.0, frequency=0.1)

    assert simulate(src, 1.0, 9) == approx_array([-0.8, -0.6, -0.4, -0.2, 0.0, 0.2, 0.4, 0.6, 0.8])

    src = CurrentSourceSawtooth(amplitude=0.5, frequency=0.1)

    assert simulate(src, 1.0, 9) == approx_array([-0.4, -0.3, -0.2, -0.1, 0.0, 0.1, 0.2, 0.3, 0.4])

    src = CurrentSourceSawtooth(amplitude=100, frequency=0.5)

    series = list(simulate(src, 0.01, 199))
    assert np.array(series[:5], dtype=float) == approx_array([-99, -98, -97, -96, -95])
    assert np.array(series[-5:], dtype=float) == approx_array([95, 96, 97, 98, 99])


def test_offset():
    src = CurrentSourceSawtooth(amplitude=5.0, frequency=100.0, offset=5.0)
    assert simulate(src, 0.001, 19) == approx_array([
        1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
    ])

    src = CurrentSourceSawtooth(amplitude=0.25, frequency=0.1, offset=-0.25)
    assert simulate(src, 1.0, 19) == approx_array([
        -0.45, -0.4, -0.35, -0.3, -0.25, -0.2, -0.15, -0.1, -0.05, -0.5,
        -0.45, -0.4, -0.35, -0.3, -0.25, -0.2, -0.15, -0.1, -0.05,
    ])

    src = CurrentSourceSawtooth(amplitude=1.0, frequency=10, offset=0.2)
    assert simulate(src, 0.005, 39) == approx_array([
        -0.7, -0.6, -0.5, -0.4, -0.3, -0.2, -0.1, 0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1,
        -0.8, -0.7, -0.6, -0.5, -0.4, -0.3, -0.2, -0.1, 0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1,
    ], abs=1e-6)


def test_phase_shift():
    # Test via periodicity property
    assert (
        simulate(CurrentSourceSawtooth(amplitude=1.0, frequency=1.0), 0.01, 100) ==
        approx_array(simulate(CurrentSourceSawtooth(amplitude=1.0, frequency=1.0, phase=2*pi), 0.01, 100))
    )

    assert (
        simulate(CurrentSourceSawtooth(amplitude=1.0, frequency=1.0, phase=0.5 * pi), 0.01, 100) ==
        approx_array(simulate(CurrentSourceSawtooth(amplitude=1.0, frequency=1.0, phase=2.5 * pi), 0.01, 100))
    )

    assert (
        simulate(CurrentSourceSawtooth(amplitude=1.0, frequency=1.0, phase=-1 * pi), 0.01, 100) ==
        approx_array(simulate(CurrentSourceSawtooth(amplitude=1.0, frequency=1.0, phase=-3 * pi), 0.01, 100))
    )


def test_event():
    voltages = [0, -5, 10, 22.3]
    amplitudes = [0, -2, 10, 333.577]
    offsets = [0, -2, 3, 1000.77]

    for v, a, o in product(voltages, amplitudes, offsets):
        src = CurrentSourceSawtooth(amplitude=a, frequency=10.0, offset=o)
        assert src.next_event(v, -0.001, 0.001) == 0.0
        assert src.next_event(v, 0.09, 0.101) == 0.1
        assert src.next_event(v, 0.04, 0.06) is None
        assert src.next_event(v, 0.124, 0.126) is None

    # Check with phase.
    for v, a, o in product(voltages, amplitudes, offsets):
        src = CurrentSourceSawtooth(amplitude=a, phase=-pi / 2, frequency=10.0, offset=o)
        assert src.next_event(v, -0.001, 0.001) is None
        assert src.next_event(v, 0.09, 0.101) is None
        assert src.next_event(v, 0.124, 0.126) == 0.125
        assert src.next_event(v, 0.224, 0.226) == 0.225
        assert src.next_event(v, 0.174, 0.176) is None


def test_effective_amplitude():
    src = CurrentSourceSawtooth(amplitude=325.0, frequency=50.0)
    assert src.effective_amplitude == approx(187.63883748662838, 1e-3)

    src = CurrentSourceSawtooth(amplitude=325.0, frequency=50.0, offset=100.0)
    assert src.effective_amplitude == approx(212.62251370288456, 1e-3)


def test_jacobian():
    src = CurrentSourceSawtooth(amplitude=10.0, frequency=50.0, offset=2.5, phase=0.2)
    assert src.get_jacobian(1000, 0, 0.01) == 0.0
