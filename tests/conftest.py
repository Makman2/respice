import csv
import os

import numpy as np
import pytest


class TestDataLoader:
    def __init__(self, path):
        self._path = path

    def load(self, filename):
        path = os.path.join(self._path, filename)
        with open(path, newline='') as fl:
            reader = csv.reader(fl)
            header = next(reader)
            return {h: np.array([float(d) for d in data]) for h, data in zip(header, zip(*reader))}


@pytest.fixture
def tstdata(request):
    return TestDataLoader(os.path.splitext(request.fspath)[0] + '_testdata')
