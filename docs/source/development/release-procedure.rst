Release Procedure
=================

This is a checklist for releasing a new version of respice:

1. Create and merge a release-commit.

   1.1. Inside ``setup.py``, increment the version.

   1.2. Commit the change with the commit message ``Prepare release v<VERSION>``,
        where ``<VERSION>`` is the same version as newly specified in the ``setup.py`` file.
        For example ``Prepare release v0.3.7``.

   1.3. Create a new Merge Request for the change.

   1.4. Wait for the tests and merge it.

2. Tag the release commit with:

   .. code:: bash

      git tag -a release/v<VERSION> -m "Release v<VERSION>"
      git push origin release/v<VERSION>

3. To be sure we work on the right commit, checkout the tag via ``git checkout release/v<VERSION>``.

4. Build and upload the package.

   4.1. Install the needed requirements:

        .. code:: bash

           python3 -m pip install -r requirements.txt -r setup-requirements.txt -r package-requirements.txt
           python3 -m pip install --upgrade twine

   4.2. Clear eventual old build artifacts by removing the ``dist`` folder.

   4.3. Build the package:

        .. code:: bash

           python3 setup.py sdist bdist_wheel

   4.4. Test the package on `test.pypi.org <https://test.pypi.org/>`_:

        4.4.1. Create a token on `test.pypi.org <https://test.pypi.org/>`_ that has access to the respice package.

        4.4.2. Upload the package:

               .. code:: bash

                  python3 -m twine upload --repository testpypi dist/*

               For the username use ``__token__`` and as the password the actual token you created earlier.

               Check for eventual error messages.

        4.4.3. Access the `project page on test.pypi.org <https://test.pypi.org/project/respice/>`_.
               Check for eventual errors in homepage links, readme text etc.

        4.4.4. *(Optional)* Remove the created token on `test.pypi.org <https://test.pypi.org/>`_ again.
               If you don't need the token frequently, rather remove it instead of keeping it around.

   4.5. Upload the package for real:

        4.5.1. Create a token on `pypi.org <https://pypi.org/>`_ that has access to the respice package.

        4.5.2. Upload the package:

               .. code:: bash

                  python3 -m twine upload dist/*

               For the username use ``__token__`` and as the password the actual token you created earlier.

               Check for eventual error messages.

        4.5.3. Access the `project page on pypi.org <https://pypi.org/project/respice/>`_.
               Check for eventual errors in homepage links, readme text etc.

        4.5.4. Create a virtual environment and make a test install inside a new folder:

               .. code:: bash

                  mkdir -p temp
                  cd temp
                  python3 -m venv venv
                  source venv/bin/activate

                  python3 -m pip install respice==<VERSION>  # E.g. respice==0.3.7
                  python3 -c "import respice"

                  deactivate
                  cd -
                  rm -rf temp

               Check for error messages.

        4.5.5. *(Optional)* Remove the created token on `pypi.org <https://pypi.org/>`_ again.
               If you don't need the token frequently, rather remove it instead of keeping it around.

References
##########

- `Packaging Python Projects (packaging.python.org) <https://packaging.python.org/en/latest/tutorials/packaging-projects/>`_

  .. note::

     The source has been updated with a newer building method using the ``build`` module.
     This does not work yet fully, and until respice has been upgraded to adhere to the newer
     packaging guide instructions, this guide uses the old way of building that manually invokes
     ``setup.py``.
