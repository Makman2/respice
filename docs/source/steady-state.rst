Steady-State and Shooting Method
================================

respice features the calculation of periodic steady-state solution.
A periodic steady-state solution occurs if all transients have settled.
A plain simulation usually takes too long, but a direct calculation with the shooting method
is faster.

Components declare their state via :attr:`respice.components.Component.state`.
