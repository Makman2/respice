.. code-block:: python

   from respice.analysis import Circuit
   from respice.components import R, L, C, VoltageSourceRectangular

   # ------------------------------------------------------------ (1)

   f = 3600

   src = VoltageSourceRectangular(amplitude=5, frequency=f,
                                  offset=5, duty=0.75)
   c1 = C(1e-9)
   l1 = L(10e-3)
   rl = R(1)

   # ------------------------------------------------------------ (2)

   src.name = 'E'
   rl.name = 'load (R1)'
   c1.name = 'C1'
   l1.name = 'L1'

   # ------------------------------------------------------------ (3)

   buck_converter = Circuit()
   buck_converter.add(src, 0, 1)
   buck_converter.add(l1, 1, 2)
   buck_converter.add(c1, 0, 2)
   buck_converter.add(rl, 0, 2)

   # ------------------------------------------------------------ (4)

   simulation = buck_converter.simulate(0, 100 / f, 1000)
   simulation.plot()

   # ------------------------------------------------------------ (5)
