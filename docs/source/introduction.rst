Introduction
============

respice is a transient non-linear circuit simulation framework.
To simulate circuits, a simulation script is coded from where the simulation methods and
capabilities of respice are invoked.

The main entry-point to simulations is the :class:`respice.analysis.Circuit`-class.
A typical script consists of following steps:

1. Import desired components.
2. Instantiate and set up components.
3. Assign names to components (optional).
4. Add components to circuit instance.
5. Call simulation methods.

.. include:: snippets/simulation-script.rst

Following simulation methods are offered:

- :meth:`respice.analysis.Circuit.simulate` - standard transient simulation
- :meth:`respice.analysis.Circuit.steadystate` - steady-state simulation
- :meth:`respice.analysis.Circuit.simulate_efm` - faster transient simulation with the EFM skipping slow dynamics
- :meth:`respice.analysis.Circuit.multirate_steadystate` - steady-state simulation technique for multi-rate systems utilizing the EFM

To utilize interactive methods, you should also install the requirements for interactivity
via either of following ways:

- ``pip3 install respice[interactive]``
- ``pip3 install -r interactive-requirements.txt``

respice supports following methods to work with your data:

- Plot results in a browser using ``Plotly``: :meth:`respice.analysis.Circuit.plot`
- Store data to CSV: :meth:`respice.analysis.Circuit.save`
- Interactive progress bar powered by rich showing status of simulation: :meth:`respice.analysis.Circuit.print`

Simulation
==========

As mentioned, respice offers various methods to simulate a circuit.
Apart from the standard simulation methods (:meth:`respice.Circuit.simulate`), respice sets also
a focus on periodic steady-state algorithms to calculate the long-term behavior of a circuit.
Instead of waiting for dynamics to settle after long, full transient simulation, these algorithms
calculate the steady-state directly with the shooting-method.

respice uses the MNA (Modified Nodal Analysis) to build system equations.
The equation systems are generated from a graph that describes circuit topology.
More precisely, a multi-edge digraph where each edge represents a single branch is used.
Multiple branches comprise a component, while branches are connected between nodes
(which can be arbitrary objects in respice as long as they are hashable; numbers, strings, ...).
This is described in more detail at :ref:`circuit-system-equation-phrasing`.

The graph is passed to the :class:`respice.analysis.MNAEquationStack`. It is responsible to build
the actual algebraic system equations (differential algebraic equations - DAEs) and optimize them.
Root solver functions are then invoked on these DAEs for a certain time step. The found solution
is then the solution to the circuit.

The solutions are put into result-objects, such as :class:`respice.analysis.TransientSimulation`.
All simulations are asynchronous in nature to enhance interactivity in scripts (e.g. live plotting, or status updates).
If you want to work with a result after it's finished, be sure to properly insert :meth:`respice.analysis.Circuit.wait`
calls. The result objects support various access to voltages, currents, powers and other metrics
for branches and components.

Testing
=======

respice is automatically tested powered by pytest. Tests are located in the ``tests`` folder.
Once the test requirements are installed via ``pip3 install -r test-requirements.txt`` you can invoke them
with

.. code:: bash

   pytest

Certain tests are marked as slow (taking about 5 seconds or more). For faster testing, these can
be excluded by

.. code:: bash

   pytest -m "not slow"
