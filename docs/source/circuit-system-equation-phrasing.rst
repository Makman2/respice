.. _circuit-system-equation-phrasing:

Circuit System Equation Phrasing
================================

According to the MNA (Modified Nodal Analysis), for each node current balance equations are created.
The :class:`respice.analysis.Circuit`-class is responsible to manage circuit topology and pass this
information down to the :class:`respice.analysis.MNAEquationStack` to generate the MNA equations, the
so called "master-equation" and its respective Jacobian.

The most fundamental building blocks are thus :class:`respice.components.Component` and
:class:`respice.components.Branch`. Different types of branches are available depending on what type
of connection is made between two nodes / voltage potentials. Following branches are available:

- :class:`respice.components.CurrentBranch`: Defines an input voltage to output current relation.
- :class:`respice.components.VoltageBranch`: Defines an input current to output voltage relation.
- :class:`respice.components.SwitchBranch`: Defines a controlled switch, that opens or closes depending on time.

A component will subclass these branches, override their behaviors to match the desired model
(resistive, inductive, or any other type of electrical behavior) and return them from inside the
:meth:`respice.components.Component.connect` method in order. The circuit class uses this information
to connect them into the whole circuit topology graph. Depending on the order, all specified branches
will receive a voltage-current vector ("coupling-vector") of all branches in the order specified.

- For a :class:`respice.components.CurrentBranch`: Adds a voltage entry into the coupling-vector of the voltage over the branch.
- For a :class:`respice.components.VoltageBranch`: Adds a current entry into the coupling-vector of the current flowing through the branch.
- For a :class:`respice.components.SwitchBranch`: Special branch, adds nothing to the coupling-vector.

The :class:`respice.components.SwitchBranch` class is a special kind of branch which connects two nodes
with a short depending on time. This kind of "switch" is utilized for many switched-mode power electronics devices.
This type of branch is handled directly inside :class:`respice.analysis.Circuit`, by regenerating different circuit
topology graphs depending on the state of all switches. If a switch is "on", its connected nodes are merged together into a single node.
If "off", it is ignored. This processed circuit topology graph is then handed to :class:`respice.analysis.MNAEquationStack`.

Solving
#######

Given a circuit topology graph, following steps are performed:

1. Identify disjoint subset graphs.
2. If no preferred ground nodes are specified, pick an arbitrary node as ground for each disjoint graph.
3. Iterate over all remaining, non-ground nodes.
4. Create current balance equations that invoke the current branch voltage/current functions depending on the solution
   yet to be found respecting coupled branches.
5. Generate the Jacobian for the same system.
6. Create an optimized Python code expression and compile it.
7. Root-solve the expression. Different root-solvers are tried in order.
8. Repeat until all steps have been calculated.

respice additionally supports "events". Events are discontinuous jumps during simulation that
must be specially handled. If events occur, additional time points are inserted to accurately
resolve an event and produce a correct result. See :meth:`respice.components.Component.next_event`.

Dynamics
########

Dynamics are handled by the author of a component. Dynamics are discretized by hand.
The trapezoidal rule is recommended:

.. math::

    y_2 = y_1 + \frac{h}{2} (f(t_1, y_1) + f(t_2, y_2))
